﻿namespace Proj2_WBednarczyk
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxLevel = new System.Windows.Forms.ComboBox();
            this.progressBarProgress = new System.Windows.Forms.ProgressBar();
            this.comboBoxCourse = new System.Windows.Forms.ComboBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.dateTimePickerBirth = new System.Windows.Forms.DateTimePicker();
            this.comboBoxGender = new System.Windows.Forms.ComboBox();
            this.textBoxTerm = new System.Windows.Forms.TextBox();
            this.textBoxYear = new System.Windows.Forms.TextBox();
            this.textBoxSurname = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.labelCourse = new System.Windows.Forms.Label();
            this.labelLevel = new System.Windows.Forms.Label();
            this.labelLabel = new System.Windows.Forms.Label();
            this.labelType = new System.Windows.Forms.Label();
            this.labelTerm = new System.Windows.Forms.Label();
            this.labelYear = new System.Windows.Forms.Label();
            this.labelBirth = new System.Windows.Forms.Label();
            this.labelGender = new System.Windows.Forms.Label();
            this.labelSurname = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(745, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 29);
            this.label1.TabIndex = 48;
            this.label1.Text = "Postęp";
            // 
            // comboBoxLevel
            // 
            this.comboBoxLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.comboBoxLevel.FormattingEnabled = true;
            this.comboBoxLevel.Items.AddRange(new object[] {
            "Inżynierski",
            "Magisterski"});
            this.comboBoxLevel.Location = new System.Drawing.Point(242, 522);
            this.comboBoxLevel.Name = "comboBoxLevel";
            this.comboBoxLevel.Size = new System.Drawing.Size(188, 37);
            this.comboBoxLevel.TabIndex = 43;
            this.comboBoxLevel.TextChanged += new System.EventHandler(this.comboBoxLevel_TextChanged);
            // 
            // progressBarProgress
            // 
            this.progressBarProgress.Location = new System.Drawing.Point(654, 58);
            this.progressBarProgress.MarqueeAnimationSpeed = 10;
            this.progressBarProgress.Maximum = 9;
            this.progressBarProgress.Name = "progressBarProgress";
            this.progressBarProgress.Size = new System.Drawing.Size(265, 44);
            this.progressBarProgress.TabIndex = 47;
            // 
            // comboBoxCourse
            // 
            this.comboBoxCourse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCourse.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.comboBoxCourse.FormattingEnabled = true;
            this.comboBoxCourse.Location = new System.Drawing.Point(567, 445);
            this.comboBoxCourse.Name = "comboBoxCourse";
            this.comboBoxCourse.Size = new System.Drawing.Size(362, 37);
            this.comboBoxCourse.TabIndex = 44;
            this.comboBoxCourse.TextChanged += new System.EventHandler(this.comboBoxCourse_TextChanged);
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonCancel.Location = new System.Drawing.Point(567, 574);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(158, 58);
            this.buttonCancel.TabIndex = 46;
            this.buttonCancel.Text = "Anuluj";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSave.Location = new System.Drawing.Point(750, 574);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(158, 58);
            this.buttonSave.TabIndex = 45;
            this.buttonSave.Text = "Zapisz";
            this.buttonSave.UseVisualStyleBackColor = false;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // comboBoxType
            // 
            this.comboBoxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxType.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Items.AddRange(new object[] {
            "W1-Wydział Architektury",
            "W2-Wydział Budownictwa Lądowego i Wodnego",
            "W3-Wydział Chemiczny",
            "W4-Wydział Elektroniki",
            "W5-Wydział Elektryczny",
            "W6-Wydział Geoinżynierii, Górnictwa i Geologii",
            "W7-Wydział Inżynierii Środowiska",
            "W8-Wydział Informatyki i Zarządzania",
            "W9-Wydział Mechaniczno-Energetyczny",
            "W10-Wydział Mechaniczny",
            "W11-Wydział Podstawowych Problemów Techniki",
            "W12-Wydział Elektroniki Mikrosystemów i Fotoniki",
            "W13-Wydział Matematyki",
            "W14-Wydział w Jeleniej Górze",
            "W15-Wydział w Wałbrzychu",
            "W16-Wydział w Legnicy"});
            this.comboBoxType.Location = new System.Drawing.Point(567, 375);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(362, 37);
            this.comboBoxType.TabIndex = 42;
            this.comboBoxType.TextChanged += new System.EventHandler(this.comboBoxType_TextChanged);
            // 
            // dateTimePickerBirth
            // 
            this.dateTimePickerBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dateTimePickerBirth.Location = new System.Drawing.Point(242, 300);
            this.dateTimePickerBirth.MaxDate = new System.DateTime(1998, 12, 31, 0, 0, 0, 0);
            this.dateTimePickerBirth.MinDate = new System.DateTime(1910, 1, 1, 0, 0, 0, 0);
            this.dateTimePickerBirth.Name = "dateTimePickerBirth";
            this.dateTimePickerBirth.Size = new System.Drawing.Size(322, 34);
            this.dateTimePickerBirth.TabIndex = 39;
            this.dateTimePickerBirth.Value = new System.DateTime(1998, 12, 31, 0, 0, 0, 0);
            this.dateTimePickerBirth.ValueChanged += new System.EventHandler(this.dateTimePickerBirth_ValueChanged);
            // 
            // comboBoxGender
            // 
            this.comboBoxGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.comboBoxGender.FormattingEnabled = true;
            this.comboBoxGender.Items.AddRange(new object[] {
            "Kobieta",
            "Mężczyzna"});
            this.comboBoxGender.Location = new System.Drawing.Point(242, 222);
            this.comboBoxGender.Name = "comboBoxGender";
            this.comboBoxGender.Size = new System.Drawing.Size(188, 37);
            this.comboBoxGender.TabIndex = 38;
            this.comboBoxGender.TextChanged += new System.EventHandler(this.comboBoxGender_TextChanged);
            // 
            // textBoxTerm
            // 
            this.textBoxTerm.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxTerm.Location = new System.Drawing.Point(242, 450);
            this.textBoxTerm.Name = "textBoxTerm";
            this.textBoxTerm.Size = new System.Drawing.Size(116, 34);
            this.textBoxTerm.TabIndex = 41;
            this.textBoxTerm.TextChanged += new System.EventHandler(this.textBoxTerm_TextChanged);
            this.textBoxTerm.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxTerm_KeyPress);
            // 
            // textBoxYear
            // 
            this.textBoxYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxYear.Location = new System.Drawing.Point(242, 375);
            this.textBoxYear.Name = "textBoxYear";
            this.textBoxYear.Size = new System.Drawing.Size(116, 34);
            this.textBoxYear.TabIndex = 40;
            this.textBoxYear.TextChanged += new System.EventHandler(this.textBoxYear_TextChanged);
            this.textBoxYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxYear_KeyPress);
            // 
            // textBoxSurname
            // 
            this.textBoxSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxSurname.Location = new System.Drawing.Point(242, 150);
            this.textBoxSurname.Name = "textBoxSurname";
            this.textBoxSurname.Size = new System.Drawing.Size(188, 34);
            this.textBoxSurname.TabIndex = 37;
            this.textBoxSurname.TextChanged += new System.EventHandler(this.textBoxSurname_TextChanged);
            this.textBoxSurname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxSurname_KeyPress);
            // 
            // textBoxName
            // 
            this.textBoxName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxName.Location = new System.Drawing.Point(242, 80);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(188, 34);
            this.textBoxName.TabIndex = 36;
            this.textBoxName.TextChanged += new System.EventHandler(this.textBoxName_TextChanged);
            this.textBoxName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxName_KeyPress);
            // 
            // labelCourse
            // 
            this.labelCourse.AutoSize = true;
            this.labelCourse.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelCourse.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCourse.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelCourse.Location = new System.Drawing.Point(436, 453);
            this.labelCourse.Name = "labelCourse";
            this.labelCourse.Size = new System.Drawing.Size(109, 29);
            this.labelCourse.TabIndex = 35;
            this.labelCourse.Text = "Kierunek";
            // 
            // labelLevel
            // 
            this.labelLevel.AutoSize = true;
            this.labelLevel.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelLevel.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelLevel.Location = new System.Drawing.Point(34, 530);
            this.labelLevel.Name = "labelLevel";
            this.labelLevel.Size = new System.Drawing.Size(96, 29);
            this.labelLevel.TabIndex = 34;
            this.labelLevel.Text = "Stopień";
            // 
            // labelLabel
            // 
            this.labelLabel.AutoSize = true;
            this.labelLabel.BackColor = System.Drawing.Color.Transparent;
            this.labelLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelLabel.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelLabel.Location = new System.Drawing.Point(12, 9);
            this.labelLabel.Name = "labelLabel";
            this.labelLabel.Size = new System.Drawing.Size(284, 46);
            this.labelLabel.TabIndex = 33;
            this.labelLabel.Text = "Uzupełnij dane";
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelType.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelType.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelType.Location = new System.Drawing.Point(436, 378);
            this.labelType.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(99, 29);
            this.labelType.TabIndex = 32;
            this.labelType.Text = "Wydział";
            // 
            // labelTerm
            // 
            this.labelTerm.AutoSize = true;
            this.labelTerm.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelTerm.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelTerm.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelTerm.Location = new System.Drawing.Point(34, 455);
            this.labelTerm.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelTerm.Name = "labelTerm";
            this.labelTerm.Size = new System.Drawing.Size(103, 29);
            this.labelTerm.TabIndex = 31;
            this.labelTerm.Text = "Semestr";
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelYear.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelYear.Location = new System.Drawing.Point(34, 380);
            this.labelYear.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(149, 29);
            this.labelYear.TabIndex = 30;
            this.labelYear.Text = "Rok Studiów";
            // 
            // labelBirth
            // 
            this.labelBirth.AutoSize = true;
            this.labelBirth.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelBirth.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelBirth.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelBirth.Location = new System.Drawing.Point(34, 305);
            this.labelBirth.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelBirth.Name = "labelBirth";
            this.labelBirth.Size = new System.Drawing.Size(178, 29);
            this.labelBirth.TabIndex = 29;
            this.labelBirth.Text = "Data Urodzenia";
            // 
            // labelGender
            // 
            this.labelGender.AutoSize = true;
            this.labelGender.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelGender.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelGender.Location = new System.Drawing.Point(34, 230);
            this.labelGender.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelGender.Name = "labelGender";
            this.labelGender.Size = new System.Drawing.Size(64, 29);
            this.labelGender.TabIndex = 28;
            this.labelGender.Text = "Płeć";
            // 
            // labelSurname
            // 
            this.labelSurname.AutoSize = true;
            this.labelSurname.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelSurname.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelSurname.Location = new System.Drawing.Point(34, 155);
            this.labelSurname.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelSurname.Name = "labelSurname";
            this.labelSurname.Size = new System.Drawing.Size(117, 29);
            this.labelSurname.TabIndex = 27;
            this.labelSurname.Text = "Nazwisko";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelName.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelName.Location = new System.Drawing.Point(34, 80);
            this.labelName.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(59, 29);
            this.labelName.TabIndex = 26;
            this.labelName.Text = "Imię";
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1004, 668);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxLevel);
            this.Controls.Add(this.progressBarProgress);
            this.Controls.Add(this.comboBoxCourse);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.comboBoxType);
            this.Controls.Add(this.dateTimePickerBirth);
            this.Controls.Add(this.comboBoxGender);
            this.Controls.Add(this.textBoxTerm);
            this.Controls.Add(this.textBoxYear);
            this.Controls.Add(this.textBoxSurname);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.labelCourse);
            this.Controls.Add(this.labelLevel);
            this.Controls.Add(this.labelLabel);
            this.Controls.Add(this.labelType);
            this.Controls.Add(this.labelTerm);
            this.Controls.Add(this.labelYear);
            this.Controls.Add(this.labelBirth);
            this.Controls.Add(this.labelGender);
            this.Controls.Add(this.labelSurname);
            this.Controls.Add(this.labelName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form3";
            this.Text = "System Kolejkowy Dziekanatu - Nowy Student";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxLevel;
        private System.Windows.Forms.ProgressBar progressBarProgress;
        private System.Windows.Forms.ComboBox comboBoxCourse;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.DateTimePicker dateTimePickerBirth;
        private System.Windows.Forms.ComboBox comboBoxGender;
        public System.Windows.Forms.TextBox textBoxTerm;
        private System.Windows.Forms.TextBox textBoxYear;
        private System.Windows.Forms.TextBox textBoxSurname;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label labelCourse;
        private System.Windows.Forms.Label labelLevel;
        private System.Windows.Forms.Label labelLabel;
        private System.Windows.Forms.Label labelType;
        private System.Windows.Forms.Label labelTerm;
        private System.Windows.Forms.Label labelYear;
        private System.Windows.Forms.Label labelBirth;
        private System.Windows.Forms.Label labelGender;
        private System.Windows.Forms.Label labelSurname;
        private System.Windows.Forms.Label labelName;
    }
}