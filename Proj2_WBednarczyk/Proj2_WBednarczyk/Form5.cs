﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proj2_WBednarczyk
{
    public partial class Form5 : Form
    {
        int dl1=0, dl2=0, dl3=0, dl4=0;

        public Form5()
        {
            InitializeComponent();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (textBoxID.Text != "" && textBoxTopic.Text != "" && textBoxText.Text != "" &&
        textBoxWhat.Text != "")
            {
                string topic, what, text;
                int id = Convert.ToInt32(textBoxID.Text);
                topic = textBoxTopic.Text;
                what = textBoxWhat.Text;
                text = textBoxText.Text;
                Global.globalDziekanat.nowaSprawa("Podanie", topic, what, text, id);
                MessageBox.Show("Podanie zostało złożone \nProsimy czekać na rozpatrzenie", "Dziękujemy", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
                MessageBox.Show("Dane nie są kompletne \nUzupełnij dane", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void textBoxID_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void textBoxTopic_TextChanged(object sender, EventArgs e)
        {
            int dll = textBoxTopic.TextLength;
            if ((dl1 == 0) && (dll == 1)) progressBarProgress.Increment(1);
            if (textBoxTopic.Text == "") progressBarProgress.Increment(-1);
            dl1 = dll;
        }

        private void textBoxWhat_TextChanged(object sender, EventArgs e)
        {
            int dll = textBoxWhat.TextLength;
            if ((dl2 == 0) && (dll == 1)) progressBarProgress.Increment(1);
            if (textBoxWhat.Text == "") progressBarProgress.Increment(-1);
            dl2 = dll;
        }

        private void textBoxText_TextChanged(object sender, EventArgs e)
        {
            int dll = textBoxText.TextLength;
            if ((dl3 == 0) && (dll == 1)) progressBarProgress.Increment(1);
            if (textBoxText.Text == "") progressBarProgress.Increment(-1);
            dl3 = dll;
        }

        private void textBoxID_TextChanged(object sender, EventArgs e)
        {
            int dll = textBoxID.TextLength;
            if ((dl4 == 0) && (dll == 1)) progressBarProgress.Increment(1);
            if (textBoxID.Text == "") progressBarProgress.Increment(-1);
            dl4 = dll;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

    }
}
