﻿namespace Proj2_WBednarczyk
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form4));
            this.buttonCancel = new System.Windows.Forms.Button();
            this.labelOption = new System.Windows.Forms.Label();
            this.buttonQuestionC = new System.Windows.Forms.Button();
            this.buttonQuestionB = new System.Windows.Forms.Button();
            this.buttonQuestionA = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonCancel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonCancel.Location = new System.Drawing.Point(280, 264);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(183, 59);
            this.buttonCancel.TabIndex = 7;
            this.buttonCancel.Text = "Anuluj";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // labelOption
            // 
            this.labelOption.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.labelOption.AutoSize = true;
            this.labelOption.BackColor = System.Drawing.Color.Transparent;
            this.labelOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelOption.Location = new System.Drawing.Point(162, 55);
            this.labelOption.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelOption.Name = "labelOption";
            this.labelOption.Size = new System.Drawing.Size(456, 51);
            this.labelOption.TabIndex = 8;
            this.labelOption.Text = "Wybierz rodzaj sprawy";
            // 
            // buttonQuestionC
            // 
            this.buttonQuestionC.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonQuestionC.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonQuestionC.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonQuestionC.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonQuestionC.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonQuestionC.Location = new System.Drawing.Point(524, 175);
            this.buttonQuestionC.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.buttonQuestionC.Name = "buttonQuestionC";
            this.buttonQuestionC.Size = new System.Drawing.Size(183, 59);
            this.buttonQuestionC.TabIndex = 6;
            this.buttonQuestionC.Text = "Zapytanie";
            this.buttonQuestionC.UseVisualStyleBackColor = false;
            this.buttonQuestionC.Click += new System.EventHandler(this.buttonQuestionC_Click);
            // 
            // buttonQuestionB
            // 
            this.buttonQuestionB.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonQuestionB.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonQuestionB.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonQuestionB.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonQuestionB.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonQuestionB.Location = new System.Drawing.Point(280, 175);
            this.buttonQuestionB.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.buttonQuestionB.Name = "buttonQuestionB";
            this.buttonQuestionB.Size = new System.Drawing.Size(183, 59);
            this.buttonQuestionB.TabIndex = 5;
            this.buttonQuestionB.Text = "Wniosek";
            this.buttonQuestionB.UseVisualStyleBackColor = false;
            this.buttonQuestionB.Click += new System.EventHandler(this.buttonQuestionB_Click);
            // 
            // buttonQuestionA
            // 
            this.buttonQuestionA.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonQuestionA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonQuestionA.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonQuestionA.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonQuestionA.ForeColor = System.Drawing.SystemColors.ControlText;
            this.buttonQuestionA.Location = new System.Drawing.Point(36, 175);
            this.buttonQuestionA.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.buttonQuestionA.Name = "buttonQuestionA";
            this.buttonQuestionA.Size = new System.Drawing.Size(183, 59);
            this.buttonQuestionA.TabIndex = 4;
            this.buttonQuestionA.Text = "Podanie";
            this.buttonQuestionA.UseVisualStyleBackColor = false;
            this.buttonQuestionA.Click += new System.EventHandler(this.buttonQuestionA_Click);
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(760, 381);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.labelOption);
            this.Controls.Add(this.buttonQuestionC);
            this.Controls.Add(this.buttonQuestionB);
            this.Controls.Add(this.buttonQuestionA);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form4";
            this.Text = "System Kolejkowy Dziekanatu - Kreator Sprawy";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelOption;
        private System.Windows.Forms.Button buttonQuestionC;
        private System.Windows.Forms.Button buttonQuestionB;
        private System.Windows.Forms.Button buttonQuestionA;
    }
}