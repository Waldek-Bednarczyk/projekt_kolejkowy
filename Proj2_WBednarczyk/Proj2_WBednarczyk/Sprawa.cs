﻿using System;
using System.Collections.Generic;

namespace Proj2_WBednarczyk
{
    partial class Dziekanat
    {
        internal class Sprawa
        {
            protected string ID_SPRAWY;
            protected int ID_STUD;
            protected string nazwaSprawy;           

            public Sprawa() { }
            public string noweIDSprawy()
            {
                DateTime dateTime = DateTime.Now;
                string now = dateTime.ToString();
                ID_SPRAWY = now + ID_STUD;
                return ID_SPRAWY;
            }

            public virtual List<string> daneCharakterystyczne()
            {
                List<string> data = new List<string>(6);
                return data;
            }

        }
    }
}
