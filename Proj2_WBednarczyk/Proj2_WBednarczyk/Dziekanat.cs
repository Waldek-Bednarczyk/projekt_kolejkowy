﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Proj2_WBednarczyk
{
    public partial class Dziekanat
    {
        private List<Sprawa> sprawy = new List<Sprawa>();
        private List<Student> studenci = new List<Student>();

        public Dziekanat() { }

        public void wypiszSprawy()
        {
            //Ścieżka do pulpitu
            string createFolder = Environment.GetFolderPath(
                 System.Environment.SpecialFolder.DesktopDirectory);
            //Zapis do pliku
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(createFolder + @"\sprawy.txt", false))
            {
                foreach (var sprawa in sprawy)
                    file.WriteLine(sprawa);
            }
        }
        public int wczytajDaneZPliku(string path, string name)
        {
            if (name == "studenci")
            {
                string imie, nazwisko, plec, rokUrodzenia, kierunek ="", wydzial ="";
                int rokStudiow, semestr, ID_STD, stopien, ileRazy = 0, i = 0;
                using (StreamReader reader = new StreamReader(path))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        string[] words = line.Split();
                        imie = words[i + 1];
                        line = reader.ReadLine();
                        words = line.Split();
                        nazwisko = words[i + 1];
                        line = reader.ReadLine();
                        words = line.Split();
                        plec = words[i + 1];
                        line = reader.ReadLine();
                        words = line.Split();
                        rokUrodzenia = words[i + 2] + " " + words[i + 3] + " " + words[i + 4] + " " + words[i + 5];
                        line = reader.ReadLine();
                        words = line.Split();
                        rokStudiow = Convert.ToInt32(words[i + 2]);
                        line = reader.ReadLine();
                        words = line.Split();
                        semestr = Convert.ToInt32(words[i + 1]);
                        line = reader.ReadLine();
                        words = line.Split();
                        for(int j=1; j<words.Length; j++)
                        {
                            wydzial += words[j];
                            if(j<words.Length-1)
                            {
                                wydzial += " ";
                            }
                        }
                        line = reader.ReadLine();
                        words = line.Split();
                        stopien = Convert.ToInt32(words[i + 1]);
                        line = reader.ReadLine();
                        words = line.Split();
                        for (int j = 1; j < words.Length; j++)
                        {
                            kierunek += words[j];
                            if (j < words.Length - 1)
                            {
                                kierunek += " ";
                            }
                        }
                        line = reader.ReadLine();
                        words = line.Split();
                        ID_STD = Convert.ToInt32(words[i + 1]);
                        ID_STD -= 23500;
                        if (imie != "")
                        {
                            studenci.Add(new Student(imie, nazwisko, plec, rokUrodzenia, rokStudiow, semestr, wydzial, stopien, kierunek, ID_STD));
                            ileRazy++;
                        }
                        imie = "";
                        wydzial = "";
                        kierunek = "";
                    }
                }
                File.Delete(path);
                return ileRazy;
            }
                       
            if (name == "sprawy")
            {
                string nazwaSprawy, ID_SPR = "", data1="", data2= "", data3 ="";
                int ID_STD;
                using (StreamReader reader = new StreamReader(path))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        string[] words = line.Split();
                        nazwaSprawy = words[2];
                        line = reader.ReadLine();
                        words = line.Split();
                        for (int j = 1; j < words.Length; j++)
                        {
                            ID_SPR += words[j];
                            if (j < words.Length - 1)
                            {
                                ID_SPR += " ";
                            }
                        }
                        line = reader.ReadLine();
                        words = line.Split();
                        ID_STD = Convert.ToInt32(words[2]);
                        line = reader.ReadLine();
                        words = line.Split();
                        for (int j = 1; j < words.Length; j++)
                        {
                            data1 += words[j];
                            if (j < words.Length - 1)
                            {
                                data1 += " ";
                            }
                        }
                        line = reader.ReadLine();
                        words = line.Split();
                        for (int j = 1; j < words.Length; j++)
                        {
                            data2 += words[j];
                            if (j < words.Length - 1)
                            {
                                data2 += " ";
                            }
                        }
                        line = reader.ReadLine();
                        words = line.Split();
                        for (int j = 1; j < words.Length; j++)
                        {
                            data3 += words[j];
                            if (j < words.Length - 1)
                            {
                                data3 += " ";
                            }
                        }
                        if (nazwaSprawy != "")
                        {
                            nowaSprawa(nazwaSprawy, data1, data2, data3, ID_STD);
                        }
                        nazwaSprawy = "";
                        data1 = "";
                        data2 = "";
                        data3 = "";
                    } 
                }
                File.Delete(path);
                return 0;
            }
            return 0;
        }

        public void nowaSprawa(string name, string a, string b, string c, int number)
        {
            if (name == "Podanie")
            {
                sprawy.Add(new Podanie(a, b, c, number));
            }
            if (name == "Wniosek")
            {
                sprawy.Add(new Wniosek(a, b, c, number));
            }
            if (name == "Zapytanie")
            {
                sprawy.Add(new Zapytanie(a, b, c, number));
            }
        }
        public void nowyStudent(string name, string surname, string gender, string yearBirth,
        int yearStudy, int term, string type, int level, string course, int i)
        {
            studenci.Add(new Student(name, surname, gender, yearBirth, yearStudy, term, type, level, course, i));
        }
        public void zapiszDoPliku()
        {
            //Ścieżka do pulpitu
            string createFolder = Environment.GetFolderPath(
                 System.Environment.SpecialFolder.DesktopDirectory);
            //Zapis do pliku
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(createFolder + @"\studenci.txt", false))
            {
                foreach (var student in studenci)
                    file.WriteLine(student);
            }
        }
    }
}
