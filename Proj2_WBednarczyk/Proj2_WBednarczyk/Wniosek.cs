﻿using System;
using System.Collections.Generic;

namespace Proj2_WBednarczyk
{
    class Wniosek : Dziekanat.Sprawa
    {
	    string temat;
        string pilne;
        string tekst;
        
        public Wniosek(string ptemat, string ppilne, string ptekst, int id)
        {
            nazwaSprawy = "Wniosek";
            ID_STUD = id;
            ID_SPRAWY = noweIDSprawy();
            temat = ptemat;
            pilne = ppilne;
            tekst = ptekst;
        }

        public override List<string> daneCharakterystyczne()
        {
            string idStd = ID_STUD.ToString();
            List<string> data = new List<string>(6);
            data.Add(nazwaSprawy);
            data.Add(ID_SPRAWY);
            data.Add(idStd);
            data.Add(temat);
            data.Add(pilne);
            data.Add(tekst);
            return data;
        }

        public override string ToString()
        {
            List<string> temp = new List<string>(6);
            temp = daneCharakterystyczne();
            return "Rodzaj Sprawy: " + temp[0] + Environment.NewLine + "ID Sprawy: " + temp[1] +
                Environment.NewLine + "ID Studenta: " + temp[2] + Environment.NewLine + "Temat: " +
                temp[3] + Environment.NewLine + "Pilne: " + temp[4] + Environment.NewLine +
                "Tekst: " + temp[5];
        }
    }
}
