﻿using System;
using System.Collections.Generic;

namespace Proj2_WBednarczyk
{
    class Podanie : Dziekanat.Sprawa
    {
	    string temat;
        string tekst;
        string oCo;

	    public Podanie(string ptemat, string ptekst, string poCo, int id)
        {
            nazwaSprawy = "Podanie";
            ID_STUD = id;
            ID_SPRAWY = noweIDSprawy();
            temat = ptemat;
            tekst = ptekst;
            oCo = poCo;
        }

        public override List<string> daneCharakterystyczne()
        {
            string idStd = ID_STUD.ToString();
            List<string> data = new List<string>(6);
            data.Add(nazwaSprawy);
            data.Add(ID_SPRAWY);
            data.Add(idStd);
            data.Add(temat);
            data.Add(tekst);
            data.Add(oCo);
            return data;
        }

        public override string ToString()
        {
            List<string> temp = new List<string>(6);
            temp = daneCharakterystyczne();
            return "Rodzaj Sprawy: " + temp[0] + Environment.NewLine + "ID Sprawy: " + temp[1] +
                Environment.NewLine + "ID Studenta: " + temp[2] + Environment.NewLine + "Temat: " +
                temp[3] + Environment.NewLine + "Tekst: " + temp[4] + Environment.NewLine +
                "OCo: " + temp[5];
        }
    }
}
