﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proj2_WBednarczyk
{
    public partial class Form3 : Form
    {
        int dl1 = 0, dl2 = 0, dl3 = 0, dl4 = 0, dl5 = 0, dl6 = 0, dl7 = 0, dl8 = 0, dl9 = 0;

        public Form3()
        {
            InitializeComponent();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (textBoxYear.Text != "" && textBoxTerm.Text != "" &&
    textBoxName.Text != "" && textBoxSurname.Text != "" && comboBoxGender.Text != "" &&
    comboBoxType.Text != "" && comboBoxCourse.Text != "" && comboBoxLevel.Text != "")
            {
                int yearStudy = Convert.ToInt32(textBoxYear.Text);
                int term = Convert.ToInt32(textBoxYear.Text);
                int level=0;
                if (comboBoxLevel.Text == "Inżynierski") level = 1;
                if (comboBoxLevel.Text == "Magisterski") level = 2;
                string name, surname, gender, yearBirth, type, course;
                name = textBoxName.Text;
                surname = textBoxSurname.Text;
                gender = comboBoxGender.Text;
                yearBirth = dateTimePickerBirth.Text;
                type = comboBoxType.Text;
                course = comboBoxCourse.Text;
                Global.globalDziekanat.nowyStudent(name, surname, gender, yearBirth, yearStudy, term, type, level, course, Global.i);
                Global.i++;
                MessageBox.Show("Dodawanie studenta zakończone powodzniem", "Dziękujemy", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = DialogResult.OK;
            }
            else
                MessageBox.Show("Dane nie są kompletne \n Uzupełnij dane", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void textBoxName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = char.IsNumber(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void textBoxSurname_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = char.IsNumber(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void textBoxYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsNumber(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void textBoxTerm_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsNumber(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void comboBoxType_TextChanged(object sender, EventArgs e)
        {
            if (comboBoxType.Text != "" && (dl9 == 0)) progressBarProgress.Increment(1);
            dl9++;
            switch (comboBoxType.SelectedIndex)
            {
                case 0:
                    comboBoxCourse.Items.Add("Architektura");
                    comboBoxCourse.Items.Add("Gospodarka Przestrzenna");
                    break;
                case 1:
                    comboBoxCourse.Items.Add("Budownictwo");
                    comboBoxCourse.Items.Add("Budownictwo w języku Angielskim");
                    break;

                case 2:
                    comboBoxCourse.Items.Add("Biotechnologia");
                    comboBoxCourse.Items.Add("Chemia i Analityka Przemysłowa");
                    comboBoxCourse.Items.Add("Inżynieria Chemiczna i Procesowa");
                    comboBoxCourse.Items.Add("Inżynieria Chemiczna i Procesowa w języku Angielskim");
                    comboBoxCourse.Items.Add("Inżynieria Materiałowa");
                    comboBoxCourse.Items.Add("Technologia Chemiczna");
                    break;

                case 3:
                    comboBoxCourse.Items.Add("Automatyka i Robotyka");
                    comboBoxCourse.Items.Add("Cyberbezpieczeństwo");
                    comboBoxCourse.Items.Add("Elektronika");
                    comboBoxCourse.Items.Add("Informatyka");
                    comboBoxCourse.Items.Add("Inżynieria Elektroniczna i Komputerowa");
                    comboBoxCourse.Items.Add("Teleinformatyka");
                    comboBoxCourse.Items.Add("Telekomunikacja");
                    break;

                case 4:
                    comboBoxCourse.Items.Add("Automatyka i Robotyka");
                    comboBoxCourse.Items.Add("Elektronika");
                    comboBoxCourse.Items.Add("Elektrotechnika");
                    comboBoxCourse.Items.Add("Elektrotechnika w języku Angielskim");
                    comboBoxCourse.Items.Add("Mechatronika");
                    break;

                case 5:
                    comboBoxCourse.Items.Add("Geodezja i Kartografia");
                    comboBoxCourse.Items.Add("Górnictwo i Geologia");
                    comboBoxCourse.Items.Add("Górnictwo i Geologia w języku Angielskim");
                    break;

                case 6:
                    comboBoxCourse.Items.Add("Inżynieria Środowiska");
                    comboBoxCourse.Items.Add("Technologie Ochrony Środowiska");
                    break;

                case 7:
                    comboBoxCourse.Items.Add("Informatyka");
                    comboBoxCourse.Items.Add("Informatyka w języku Angielskim");
                    comboBoxCourse.Items.Add("Inżynieria Systemów");
                    comboBoxCourse.Items.Add("Inżynieria Zarządzania");
                    comboBoxCourse.Items.Add("Zarządzanie");
                    comboBoxCourse.Items.Add("Zarządzanie w języku Angielskim");
                    break;

                case 8:
                    comboBoxCourse.Items.Add("Energetyka");
                    comboBoxCourse.Items.Add("Mechanika-i-Budowa-Maszyn");
                    break;

                case 9:
                    comboBoxCourse.Items.Add("Automatyka i Robotyka");
                    comboBoxCourse.Items.Add("Inżynieria Biomedyczna");
                    comboBoxCourse.Items.Add("Mechanika i Budowa Maszyn");
                    comboBoxCourse.Items.Add("Mechanika i Budowa Maszyn w języku Angielskim");
                    comboBoxCourse.Items.Add("Mechatronika");
                    comboBoxCourse.Items.Add("Transport");
                    comboBoxCourse.Items.Add("Zarządzanie i Inżynieria produkcji");
                    break;

                case 10:
                    comboBoxCourse.Items.Add("Fizyka Techniczna");
                    comboBoxCourse.Items.Add("Informatyka");
                    comboBoxCourse.Items.Add("Inżynieria Biomedyczna");
                    comboBoxCourse.Items.Add("Inżynieria Kwantowa");
                    comboBoxCourse.Items.Add("Optyka");
                    break;

                case 11:
                    comboBoxCourse.Items.Add("Elektronika i Telekomunikacja");
                    comboBoxCourse.Items.Add("Mechatronika");
                    break;

                case 12:
                    comboBoxCourse.Items.Add("Matematyka");
                    comboBoxCourse.Items.Add("Matematyka i Statystyka");
                    comboBoxCourse.Items.Add("Matematyka Stosowana");
                    break;

                case 13:
                    comboBoxCourse.Items.Add("Informatyka Przemysłowa");
                    break;

                case 14:
                    comboBoxCourse.Items.Add("Mechatronika Pojazdów");
                    break;

                case 15:
                    comboBoxCourse.Items.Add("Budowa Maszyn i Pojazdów");
                    comboBoxCourse.Items.Add("Inżynieria Odnawialnych Źródeł Energii");
                    break;
            }
        }

        private void textBoxName_TextChanged(object sender, EventArgs e)
        {
            int dll = textBoxName.TextLength;
            if ((dl5 == 0) && (dll == 1)) progressBarProgress.Increment(1);
            if (textBoxName.Text == "") progressBarProgress.Increment(-1);
            dl5 = dll;
        }

        private void textBoxSurname_TextChanged(object sender, EventArgs e)
        {
            int dll = textBoxSurname.TextLength;
            if ((dl4 == 0) && (dll == 1)) progressBarProgress.Increment(1);
            if (textBoxSurname.Text == "") progressBarProgress.Increment(-1);
            dl4 = dll;
        }

        private void comboBoxGender_TextChanged(object sender, EventArgs e)
        {
            if (dl3 == 0) progressBarProgress.Increment(1);
            dl3++;
        }

        private void dateTimePickerBirth_ValueChanged(object sender, EventArgs e)
        {
            if (dl8 == 0) progressBarProgress.Increment(1);
            dl8++;
        }

        private void textBoxYear_TextChanged(object sender, EventArgs e)
        {
            int dll = textBoxYear.TextLength;
            if ((dl2 == 0) && (dll == 1)) progressBarProgress.Increment(1);
            if (textBoxYear.Text == "") progressBarProgress.Increment(-1);
            dl2 = dll;
            int temp;
            Int32.TryParse(textBoxYear.Text, out temp);
            if (temp > 5)
            {
                MessageBox.Show("Maksymalny rok to 5", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxYear.Text = "";
            }
        }

        private void textBoxTerm_TextChanged(object sender, EventArgs e)
        {
            int dll = textBoxTerm.TextLength;
            if ((dl1 == 0) && (dll == 1)) progressBarProgress.Increment(1);
            if (textBoxTerm.Text == "") progressBarProgress.Increment(-1);
            dl1 = dll;
            int temp;
            Int32.TryParse(textBoxTerm.Text,out temp);
            if (temp > 7)
            {
                MessageBox.Show("Maksymalny semestr to 7", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxTerm.Text = "";
            }
        }

        private void comboBoxLevel_TextChanged(object sender, EventArgs e)
        {
            if (dl6 == 0) progressBarProgress.Increment(1);
            dl6++;
        }

        private void comboBoxCourse_TextChanged(object sender, EventArgs e)
        {
            if (comboBoxCourse.Text != "" && (dl7 == 0)) progressBarProgress.Increment(1);
            dl7++;
        }
    }
}
