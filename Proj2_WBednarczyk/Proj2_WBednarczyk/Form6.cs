﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proj2_WBednarczyk
{
    public partial class Form6 : Form
    {
        int dl1=0, dl2=0, dl3=0;
        public Form6()
        {
            InitializeComponent();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (textBoxID.Text != "" && textBoxTopic.Text != "" && textBoxTopic.Text != "")
            {
                string topic, text, pilne;
                int id = Convert.ToInt32(textBoxID.Text);
                topic = textBoxTopic.Text;
                text = textBoxText.Text;
                if (checkBoxFast.Checked) pilne = "Tak"; else pilne = "Nie";
                Global.globalDziekanat.nowaSprawa("Wniosek", topic, pilne, text, id);
                MessageBox.Show("Wniosek został złożony \nProsimy oczekiwać na rozpatrzenie", "Dziękujemy", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = DialogResult.OK;
            }
            else
                MessageBox.Show("Dane nie są kompletne \nUzupełnij dane", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void textBoxID_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void textBoxTopic_TextChanged(object sender, EventArgs e)
        {
            int dll = textBoxTopic.TextLength;
            if ((dl1 == 0) && (dll == 1)) progressBarProgress.Increment(1);
            if (textBoxTopic.Text == "") progressBarProgress.Increment(-1);
            dl1 = dll;
        }

        private void textBoxText_TextChanged(object sender, EventArgs e)
        {
            int dll = textBoxText.TextLength;
            if ((dl2 == 0) && (dll == 1)) progressBarProgress.Increment(1);
            if (textBoxText.Text == "") progressBarProgress.Increment(-1);
            dl2 = dll;
        }

        private void textBoxID_TextChanged(object sender, EventArgs e)
        {
            int dll = textBoxID.TextLength;
            if ((dl3 == 0) && (dll == 1)) progressBarProgress.Increment(1);
            if (textBoxID.Text == "") progressBarProgress.Increment(-1);
            dl3 = dll;
        }
    }
}
