﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proj2_WBednarczyk
{
    public partial class Form7 : Form
    {
        int dl1 = 0, dl2 = 0, dl3 = 0, dl4 = 0;
        public Form7()
        {
            InitializeComponent();
        }

        private void textBoxID_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void textBoxID_TextChanged(object sender, EventArgs e)
        {
            int dll = textBoxID.TextLength;
            if ((dl4 == 0) && (dll == 1)) progressBarProgress.Increment(1);
            if (textBoxID.Text == "") progressBarProgress.Increment(-1);
            dl4 = dll;
        }

        private void textBoxText_TextChanged(object sender, EventArgs e)
        {
            int dll = textBoxText.TextLength;
            if ((dl3 == 0) && (dll == 1)) progressBarProgress.Increment(1);
            if (textBoxText.Text == "") progressBarProgress.Increment(-1);
            dl3 = dll;
        }

        private void textBoxQuestion_TextChanged(object sender, EventArgs e)
        {
            int dll = textBoxQuestion.TextLength;
            if ((dl2 == 0) && (dll == 1)) progressBarProgress.Increment(1);
            if (textBoxQuestion.Text == "") progressBarProgress.Increment(-1);
            dl2 = dll;
        }

        private void textBoxWho_TextChanged(object sender, EventArgs e)
        {
            int dll = textBoxWho.TextLength;
            if ((dl1 == 0) && (dll == 1)) progressBarProgress.Increment(1);
            if (textBoxWho.Text == "") progressBarProgress.Increment(-1);
            dl1 = dll;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (textBoxWho.Text != "" && textBoxQuestion.Text != "" &&
        textBoxText.Text != "" && textBoxID.Text != "")
            {
                string who, question, text;
                int id = Convert.ToInt32(textBoxID.Text);
                who = textBoxWho.Text;
                question = textBoxQuestion.Text;
                text = textBoxText.Text;
                Global.globalDziekanat.nowaSprawa("Zapytanie", who, question, text, id);
                MessageBox.Show("Zapytanie zostało wysłane \nProsimy czekać na odpowiedź", "Dziękujemy", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = DialogResult.OK;
            }
            else
                MessageBox.Show("Dane nie są kompletne \nUzupełnij dane", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
    }
}
