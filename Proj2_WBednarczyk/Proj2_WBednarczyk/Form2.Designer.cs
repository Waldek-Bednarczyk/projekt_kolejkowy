﻿namespace Proj2_WBednarczyk
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.label1 = new System.Windows.Forms.Label();
            this.buttonEnd = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonSaveStudent = new System.Windows.Forms.Button();
            this.buttonNewCause = new System.Windows.Forms.Button();
            this.buttonNewStudent = new System.Windows.Forms.Button();
            this.buttonRead = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(412, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 46);
            this.label1.TabIndex = 14;
            this.label1.Text = "Menu";
            // 
            // buttonEnd
            // 
            this.buttonEnd.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonEnd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonEnd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonEnd.Location = new System.Drawing.Point(356, 351);
            this.buttonEnd.Margin = new System.Windows.Forms.Padding(7);
            this.buttonEnd.Name = "buttonEnd";
            this.buttonEnd.Size = new System.Drawing.Size(228, 93);
            this.buttonEnd.TabIndex = 13;
            this.buttonEnd.Text = "Zakończ";
            this.buttonEnd.UseVisualStyleBackColor = false;
            this.buttonEnd.Click += new System.EventHandler(this.buttonEnd_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSave.Location = new System.Drawing.Point(665, 110);
            this.buttonSave.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(228, 93);
            this.buttonSave.TabIndex = 12;
            this.buttonSave.Text = "Zapisz Sprawy";
            this.buttonSave.UseVisualStyleBackColor = false;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonSaveStudent
            // 
            this.buttonSaveStudent.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonSaveStudent.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSaveStudent.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSaveStudent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSaveStudent.Location = new System.Drawing.Point(665, 235);
            this.buttonSaveStudent.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.buttonSaveStudent.Name = "buttonSaveStudent";
            this.buttonSaveStudent.Size = new System.Drawing.Size(228, 93);
            this.buttonSaveStudent.TabIndex = 11;
            this.buttonSaveStudent.Text = "Zapisz Studentów";
            this.buttonSaveStudent.UseVisualStyleBackColor = false;
            this.buttonSaveStudent.Click += new System.EventHandler(this.buttonSaveStudent_Click);
            // 
            // buttonNewCause
            // 
            this.buttonNewCause.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonNewCause.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonNewCause.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonNewCause.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonNewCause.Location = new System.Drawing.Point(45, 235);
            this.buttonNewCause.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.buttonNewCause.Name = "buttonNewCause";
            this.buttonNewCause.Size = new System.Drawing.Size(228, 93);
            this.buttonNewCause.TabIndex = 10;
            this.buttonNewCause.Text = "Dodaj nową sprawę";
            this.buttonNewCause.UseVisualStyleBackColor = false;
            this.buttonNewCause.Click += new System.EventHandler(this.buttonNewCause_Click);
            // 
            // buttonNewStudent
            // 
            this.buttonNewStudent.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonNewStudent.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonNewStudent.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonNewStudent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonNewStudent.Location = new System.Drawing.Point(45, 110);
            this.buttonNewStudent.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.buttonNewStudent.Name = "buttonNewStudent";
            this.buttonNewStudent.Size = new System.Drawing.Size(228, 93);
            this.buttonNewStudent.TabIndex = 9;
            this.buttonNewStudent.Text = "Dodaj nowego studenta";
            this.buttonNewStudent.UseVisualStyleBackColor = false;
            this.buttonNewStudent.Click += new System.EventHandler(this.buttonNewStudent_Click);
            // 
            // buttonRead
            // 
            this.buttonRead.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonRead.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonRead.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonRead.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonRead.Location = new System.Drawing.Point(356, 167);
            this.buttonRead.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.buttonRead.Name = "buttonRead";
            this.buttonRead.Size = new System.Drawing.Size(228, 93);
            this.buttonRead.TabIndex = 8;
            this.buttonRead.Text = "Wczytaj dane z pliku";
            this.buttonRead.UseVisualStyleBackColor = false;
            this.buttonRead.Click += new System.EventHandler(this.buttonRead_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(953, 494);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonEnd);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonSaveStudent);
            this.Controls.Add(this.buttonNewCause);
            this.Controls.Add(this.buttonNewStudent);
            this.Controls.Add(this.buttonRead);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form2";
            this.Text = "System Kolejokowy Dziekanatu - Menu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonEnd;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonSaveStudent;
        private System.Windows.Forms.Button buttonNewCause;
        private System.Windows.Forms.Button buttonNewStudent;
        private System.Windows.Forms.Button buttonRead;
    }
}