﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proj2_WBednarczyk
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void buttonNewStudent_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3();
            this.Hide();
            form3.ShowDialog();
            this.Show();
        }

        private void buttonEnd_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            Global.globalDziekanat.wypiszSprawy();
            MessageBox.Show("Zapisywanie zakończone powodzeniem \nZapisany plik znajduje się na pulpicie", "Zapisano", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void buttonSaveStudent_Click(object sender, EventArgs e)
        {
            Global.globalDziekanat.zapiszDoPliku();
            MessageBox.Show("Zapisywanie zakończone powodzeniem \nZapisany plik znajduje się na pulpicie", "Zapisano", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void buttonNewCause_Click(object sender, EventArgs e)
        {
            Form4 form4 = new Form4();
            this.Hide();
            form4.ShowDialog();
            this.Show();
        }

        private void buttonRead_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aby wczytać sprawy wybierz plik sprawy.txt \nAby wczytać studentów wybierz plik studenci.txt", "Wczytywanie",
            MessageBoxButtons.OK, MessageBoxIcon.Information);
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = "C:\\";
            openFileDialog1.RestoreDirectory = true;
            string path, name;
            openFileDialog1.ShowDialog();
            path = openFileDialog1.FileName;
            name = openFileDialog1.SafeFileName;
            path.Replace("\\", "\\\\");
            if (name == "studenci.txt" || name == "sprawy.txt")
            {
                if (name == "sprawy.txt")
                {
                    Global.i+= Global.globalDziekanat.wczytajDaneZPliku(path, "sprawy");
                    MessageBox.Show("Wczytywanie spraw zakończone powodzeniem \nPlik został usunięty", "Wczytano", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                if (name == "studenci.txt")
                {
                    Global.i+= Global.globalDziekanat.wczytajDaneZPliku(path, "studenci");
                    MessageBox.Show("Wczytywanie studentów zakończone powodzeniem \nPlik został usunięty", "Wczytano", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
                MessageBox.Show("Nie wczytano\nNiepoprawna nazwa pliku", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
