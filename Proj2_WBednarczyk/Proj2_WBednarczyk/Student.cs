﻿using System;

namespace Proj2_WBednarczyk
{
    partial class Dziekanat
    {
        internal class Student
        {
	        int ID_STUD;
            string imie;
            string nazwisko;
            string plec;
            string rokUrodzenia;
            int rokStudiow;
            int semestr;
            string kierunek;
            int stopien;
            string wydzial;

            public Student(string pimie, string pnazwisko, string pplec, string prokUrodzenia, int prokStudiow,
            int psemestr, string pwydzial, int pstopien, string pkierunek, int i)
            {
                ID_STUD = 23500 + i;
                imie = pimie;
                nazwisko = pnazwisko;
                plec = pplec;
                rokStudiow = prokStudiow;
                rokUrodzenia = prokUrodzenia;
                semestr = psemestr;
                kierunek = pkierunek;
                stopien = pstopien;
                wydzial = pwydzial;
            }
            
            public override string ToString()
            {
                   return "Imię: " + imie + Environment.NewLine + "Nazwisko: " + nazwisko + Environment.NewLine + "Płeć: " +
                   plec + Environment.NewLine + "Data Urodzenia: " + rokUrodzenia + Environment.NewLine + "Rok Studiów: " +
                   rokStudiow + Environment.NewLine + "Semestr: " + semestr + Environment.NewLine + "Wydział: " +
                   wydzial + Environment.NewLine + "Stopień: " + stopien + Environment.NewLine + "Kierunek: " + kierunek +
                   Environment.NewLine + "ID: " + ID_STUD;
            }

        }
    }
    
}
